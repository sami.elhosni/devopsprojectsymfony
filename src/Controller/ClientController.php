<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientType;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/client')]
class ClientController extends AbstractController
{
    #[Route('/', name: 'app_client_index', methods: ['GET'])]
    public function index(ClientRepository $clientRepository): Response
    {
        return $this->render('client/index.html.twig', [
            'clients' => $clientRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_client_new', methods: ['GET', 'POST'])]
public function new(Request $request, EntityManagerInterface $entityManager): Response
{
    $client = new Client();
    $form = $this->createForm(ClientType::class, $client);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $entityManager->persist($client);
        $entityManager->flush();

        return $this->redirectToRoute('app_client_index', [], Response::HTTP_SEE_OTHER);
    }

    return $this->render('client/new.html.twig', [
        'form' => $form->createView(),  // Ensure you pass the view, not the form itself
    ]);
}


    #[Route('/{id}', name: 'app_client_show', methods: ['GET'])]
    public function show(ClientRepository $clientRepository,$id): Response
    {          
         $client = $clientRepository->find($id);   

        return $this->render('client/show.html.twig', [
            'client' => $client,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_client_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, EntityManagerInterface $entityManager, ClientRepository $clientRepository,$id): Response
    {
        $client = $clientRepository->find($id);   
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_client_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('client/edit.html.twig', [
            'client' => $client,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'app_client_delete', methods: ['POST'])]
    public function delete(Request $request, ClientRepository $clientRepository,$id , EntityManagerInterface $entityManager): Response
    {
        $client = $clientRepository->find($id);   
            $entityManager->remove($client);
            $entityManager->flush();
        

        return $this->redirectToRoute('app_client_index', [], Response::HTTP_SEE_OTHER);
    }
}