<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Voiture;
use App\Entity\Location;
use Doctrine\DBAL\Types\Types;

class VoitureTest extends TestCase
{
    public function testGetterAndSetterMethods()
    {
        $voiture = new Voiture();

        $voiture->setSerie('123ABC');
        $voiture->setDateMM(new \DateTime('2024-01-01'));
        $voiture->setPrixJour(50.00);

        $this->assertEquals('123ABC', $voiture->getSerie());
        $this->assertEquals(new \DateTime('2024-01-01'), $voiture->getDateMM());
        $this->assertEquals(50.00, $voiture->getPrixJour());
    }

    public function testIdIsNullOnNewVoiture()
    {
        $voiture = new Voiture();
        $this->assertNull($voiture->getId());
    }

    public function testAddAndRemoveLocation()
    {
        $voiture = new Voiture();
        $location = new Location();

        $voiture->addLocation($location);
        $this->assertTrue($voiture->getLocations()->contains($location));
        $this->assertSame($voiture, $location->getVoiture());

        $voiture->removeLocation($location);
        $this->assertFalse($voiture->getLocations()->contains($location));
        $this->assertNull($location->getVoiture());
    }
}
