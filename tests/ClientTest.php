<?php

namespace App\Tests;


use PHPUnit\Framework\TestCase;
use App\Entity\Client;
use App\Entity\Location;

class ClientTest extends TestCase
{
    public function testGetterAndSetterMethods()
    {
        $client = new Client();

        $client->setNom('John');
        $client->setPrenom('Doe');
        $client->setAdresse('123 Main St');
        $client->setCin('ABC123');

        $this->assertEquals('John', $client->getNom());
        $this->assertEquals('Doe', $client->getPrenom());
        $this->assertEquals('123 Main St', $client->getAdresse());
        $this->assertEquals('ABC123', $client->getCin());
    }

    public function testIdIsNullOnNewClient()
    {
        $client = new Client();
        $this->assertNull($client->getId());
    }

    public function testAddAndRemoveLocation()
    {
        $client = new Client();
        $location = new Location();

        $client->addLocation($location);
        $this->assertTrue($client->getLocations()->contains($location));
        $this->assertSame($client, $location->getClient());

        $client->removeLocation($location);
        $this->assertFalse($client->getLocations()->contains($location));
        $this->assertNull($location->getClient());
    }
}
