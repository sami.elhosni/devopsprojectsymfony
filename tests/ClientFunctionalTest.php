<?php

namespace App\Tests;

use Symfony\Component\Panther\PantherTestCase;

class ClientFunctionalTest extends PantherTestCase
{
    public function testClientCreationAndView(): void
    {
        $client = static::createPantherClient();

        // Création d'un nouveau client
        $clientData = [
            'nom' => 'Doe',
            'prenom' => 'John',
            'adresse' => '123 Main St',
            'cin' => 'ABC123',
        ];

        $client->request('GET', '/create-client-form');  // Assurez-vous d'avoir une route correspondante dans votre application
        $client->submitForm('Save', [
            'client[nom]' => $clientData['nom'],
            'client[prenom]' => $clientData['prenom'],
            'client[adresse]' => $clientData['adresse'],
            'client[cin]' => $clientData['cin'],
        ]);

        $this->assertSelectorTextContains('div.alert', 'Client created successfully.');

        // Vérification de la page de détails du client
        $client->clickLink('View Client Details');
        $this->assertSelectorTextContains('h1', 'Client Details');
        $this->assertSelectorTextContains('td', $clientData['nom']);
        $this->assertSelectorTextContains('td', $clientData['prenom']);
        $this->assertSelectorTextContains('td', $clientData['adresse']);
        $this->assertSelectorTextContains('td', $clientData['cin']);
    }
}
