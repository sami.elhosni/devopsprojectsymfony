<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Location;
use App\Entity\Client;
use App\Entity\Voiture;

class LocationTest extends TestCase
{
    public function testGetterAndSetterMethods()
    {
        $location = new Location();
        $client = new Client();
        $voiture = new Voiture();

        $dateD = new \DateTime('2024-01-01');
        $dateA = new \DateTime('2024-01-10');

        $location->setDateD($dateD);
        $location->setDateA($dateA);
        $location->setPrix(100.50);
        $location->setClient($client);
        $location->setVoiture($voiture);

        $this->assertEquals($dateD, $location->getDateD());
        $this->assertEquals($dateA, $location->getDateA());
        $this->assertEquals(100.50, $location->getPrix());
        $this->assertSame($client, $location->getClient());
        $this->assertSame($voiture, $location->getVoiture());
    }

    public function testIdIsNullOnNewLocation()
    {
        $location = new Location();
        $this->assertNull($location->getId());
    }

    public function testDateDebutIsDate()
    {
        $location = new Location();
        $dateDebut = new \DateTime('2024-01-01');
        $location->setDateD($dateDebut);
        $this->assertInstanceOf(\DateTime::class, $location->getDateD());
    }

    public function testDateFinIsDate()
    {
        $location = new Location();
        $dateFin = new \DateTime('2024-01-10');
        $location->setDateA($dateFin);
        $this->assertInstanceOf(\DateTime::class, $location->getDateA());
    }

    public function testPrixIsNumeric()
    {
        $location = new Location();
        $location->setPrix(200.00);
        $this->assertIsNumeric($location->getPrix());
    }

    public function testVoitureManyToOne()
    {
        $location = new Location();
        $voiture = new Voiture();
        $location->setVoiture($voiture);
        $this->assertSame($voiture, $location->getVoiture());
    }
}
